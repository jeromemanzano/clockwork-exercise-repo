﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class CurrentTimeController : Controller
    {
        // GET api/currenttime
        [HttpGet]
        public IActionResult Get()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
            var timeZone = TimeZoneInfo.Local.StandardName;

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZoneName = timeZone
            };

            try
            {
                InsertToDB(returnVal);

                return Ok(returnVal);
            }
            catch (DbUpdateException)
            {
                return StatusCode(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine("error");

                return BadRequest(ex);
            }

        }

        [HttpGet("{timeZone}")]
        public IActionResult Get(string timeZone)
        {
            var timeZoneInfo = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(x => x.StandardName == timeZone);
            var currentTime = new DateTime(DateTime.Now.Ticks, DateTimeKind.Unspecified);
            var utcTime = TimeZoneInfo.ConvertTimeToUtc(currentTime, timeZoneInfo);
            var serverTime = TimeZoneInfo.ConvertTime(currentTime, timeZoneInfo);
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZoneName = timeZone
            };

            try
            {
                InsertToDB(returnVal);

                return Ok(returnVal);
            }
            catch (DbUpdateException)
            {
                return StatusCode(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine("error");

                return BadRequest(ex);
            }

        }

        private void InsertToDB(CurrentTimeQuery timeQuery)
        {
            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(timeQuery);
                var count = db.SaveChanges();
                Console.WriteLine("{0} record/s saved to database", count);

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                }
            }
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            try
            {
                List<CurrentTimeQuery> returnVal;

                using (var db = new ClockworkContext())
                {
                    returnVal = new List<CurrentTimeQuery>(db.CurrentTimeQueries.ToList());
                }

                Console.WriteLine("{0} entrie/s returned", returnVal.Count());

                return Ok(returnVal);

            }
            catch (ArgumentNullException)
            {
                return Ok("No entries");
            }
            catch (OverflowException)
            {
                return StatusCode(500);
            }
            catch (DbUpdateException)
            {
                return StatusCode(500);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
