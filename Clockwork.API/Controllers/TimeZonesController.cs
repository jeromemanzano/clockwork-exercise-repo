﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class TimeZonesController : Controller
    {
        // GET api/TimeZone
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(TimeZoneInfo.GetSystemTimeZones().Select(x => x.StandardName));
        }
    }
}
