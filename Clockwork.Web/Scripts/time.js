﻿window.onload = Initialize();



function Initialize() {

    GetServerTime();
    GetAllTimeZones();
}

function TimeZoneChanged(selectObject) {

    var newTimeZone = selectObject.value;
    GetTimeDetails("/" + newTimeZone);
}

function GetAllTimeZones() {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200 && !this.hasTimeZones) {

            var selector = document.getElementById('timeZonesList');
            var timeZones = JSON.parse(this.responseText);

            var dropList = ""

            for (var i = 0; i < timeZones.length; i++) {

                dropList += "<option>" + timeZones[i] + "</option>";
            }


            selector.innerHTML = dropList;
            selector.selectedIndex = -1;
        }
    };

    xhttp.open("GET", encodeURI("http://localhost:5000/api/timezones"), true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function UpdateTimeDisplay() {

    var timeZone = "/" + document.getElementById('timeZoneName').innerText;
    GetTimeDetails(timeZone)

    return false;
}

function GetServerTime() {

    GetTimeDetails("");
}

function GetTimeDetails(timeZone) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            UpdateDisplay(this.responseText);
        }
    };

    xhttp.open("GET", encodeURI("http://localhost:5000/api/currenttime" + timeZone), true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

function UpdateDisplay(response) {
    var item = JSON.parse(response);

    document.getElementById('localTime').innerText = item['time'];
    document.getElementById('utcTime').innerText = item['utcTime'];
    document.getElementById('timeZoneName').innerText = item['timeZoneName'];
}


function GetRecords() {


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var records = JSON.parse(this.responseText);

            var tableDetails = BuildHeader();

            for (var i = 0; i < records.length; i++) {
                var row = BuildRow(records[i]);
                tableDetails += row;
            }

            document.getElementById('recordsTable').innerHTML = tableDetails;

        }
    };

    xhttp.open("GET", encodeURI("http://localhost:5000/api/currenttime/all"), true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();

    return false
}

function BuildHeader() {
    return "<tr><th>Standard Name</th><th>Local Time</th><th>UTC Time</th></tr>";
}

function BuildRow(record) {

    return "<tr><td>" + record['timeZoneName'] + "</td><td>" + record['time'] + "</td><td>" + record['utcTime'] + "</td></tr>";
}

